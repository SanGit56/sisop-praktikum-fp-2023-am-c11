#include <dirent.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define DATABASE_ROOT "../databases"
#define PORT 8081
#define BUFFER_SIZE 1024
#define ATTRIBUTE_SIZE 64
#define MAX_ATTRIBUTE_ON_TABLE 124
#define STRING_SIZE 225

char cmd[BUFSIZ];  // command for system()

void freeWordsArray(char **words) {
    int i;
    for (i = 0; i < MAX_ATTRIBUTE_ON_TABLE; i++) {
        free(words[i]);
    }
    free(words);
}

int fileOrDirectoryExists(const char *path) {
    struct stat st;
    if (stat(path, &st) == 0) {
        if (S_ISDIR(st.st_mode)) {
            return 1;  // Directory exists
        } else if (S_ISREG(st.st_mode)) {
            return 2;  // File exists
        }
    }
    return 0;  // File or directory does not exist
}

void createTable(char database[], char table[], int attributeAmount, char **attributes) {
    char filePath[BUFSIZ];
    sprintf(filePath, "%s/%s/%s", DATABASE_ROOT, database, table);

    int result = fileOrDirectoryExists(filePath);
    if (result == 0) {  // does not exist
        sprintf(cmd, "touch '%s'", filePath);
        system(cmd);
        FILE *tableFile = fopen(filePath, "w");
        for (int j = 0; j < attributeAmount; j += 2) {
            fprintf(tableFile, "%s:%s|", attributes[j], attributes[j + 1]);
        }

        fprintf(tableFile, "\n");
        fclose(tableFile);
    } else if (result == 2) {
        printf("table %s on %s database is already exist!\n", table, database);
    }
}

int createDatabase(char databaseName[]) {
    char filePath[BUFSIZ];
    sprintf(filePath, "%s/%s", DATABASE_ROOT, databaseName);
    DIR *databaseDirectory = opendir(filePath);
    if (databaseDirectory == NULL) {
        sprintf(cmd, "mkdir -p '%s'", filePath);
        system(cmd);
        return 1;
    }
    return 0;
}

void addUser(char user[], char pass[]) {
    sprintf(cmd, "echo \"$(( $(wc -l < %s/root/user) ))|%s|%s|\" >> %s/root/user", DATABASE_ROOT, user, pass, DATABASE_ROOT);
    system(cmd);
}

void addDB(char dbName[]) {
    sprintf(cmd, "echo \"$(( $(wc -l < %s/root/database) ))|%s|\" >> %s/root/database", DATABASE_ROOT, dbName, DATABASE_ROOT);
    system(cmd);
}

void addDB_permission(char dbName[], char user[]) {
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/root/user", DATABASE_ROOT);
    FILE *file = fopen(filePath, "r");
    int userId = 0;
    char line[BUFSIZ];
    while (fgets(line, BUFSIZ, file) != NULL) {
        if (strstr(line, user)) break;
        userId++;
    }
    fclose(file);

    sprintf(filePath, "%s/root/database", DATABASE_ROOT);
    file = fopen(filePath, "r");
    int dbID = 0;
    while (fgets(line, BUFSIZ, file) != NULL) {
        if (strstr(line, dbName)) break;
        dbID++;
    }
    fclose(file);

    sprintf(cmd, "echo \"%d|%d|\" >> %s/root/database_permission", userId, dbID, DATABASE_ROOT);
    system(cmd);
}

void splitString(const char *inputString, char ***words, int *wordCount) {
    *words = (char **)malloc(MAX_ATTRIBUTE_ON_TABLE * sizeof(char *));

    int i;
    for (i = 0; i < MAX_ATTRIBUTE_ON_TABLE; i++) {
        (*words)[i] = (char *)malloc(ATTRIBUTE_SIZE * sizeof(char));
    }

    *wordCount = 0;
    int charCount = 0;

    for (i = 0; inputString[i] != '\0'; i++) {
        if (inputString[i] == ',' || inputString[i] == ' ') {
            if (inputString[i] == ',' && inputString[i + 1] == ' ') continue;
            // End of word, move to the next word
            (*words)[*wordCount][charCount] = '\0';  // Add null terminator
            (*wordCount)++;
            charCount = 0;
        } else {
            // Store the character in the current word
            (*words)[*wordCount][charCount] = inputString[i];
            charCount++;
        }
    }

    // Add null terminator for the last word
    (*words)[*wordCount][charCount] = '\0';
}

int createDatabaseSystem() {
    DIR *databaseDirectory = opendir(DATABASE_ROOT);
    if (databaseDirectory == NULL) {
        sprintf(cmd, "mkdir -p '%s'", DATABASE_ROOT);
        system(cmd);
        createDatabase("root");

        int wordCount;
        char **words;
        char inputString[BUFSIZ];

        sprintf(inputString, "id INT, username STRING, password STRING");
        splitString(inputString, &words, &wordCount);
        createTable("root", "user", wordCount, words);

        wordCount = 0;
        freeWordsArray(words);
        sprintf(inputString, "id INT, name STRING");
        splitString(inputString, &words, &wordCount);
        createTable("root", "database", wordCount, words);

        wordCount = 0;
        freeWordsArray(words);
        sprintf(inputString, "userid INT, databaseID INT");
        splitString(inputString, &words, &wordCount);
        createTable("root", "database_permission", wordCount, words);

        closedir(databaseDirectory);
        return 1;
    }
    closedir(databaseDirectory);
    return 0;
}

bool checkUserPass(char user[], char pass[]) {
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/root/user", DATABASE_ROOT);
    FILE *file = fopen(filePath, "r");

    char line[BUFSIZ];
    while (fgets(line, BUFSIZ, file) != NULL) {
        if (strstr(line, user))
            if (strstr(line, pass)) {
                fclose(file);
                return true;
            }
    }
    fclose(file);
    return false;
}

bool checkUser(char user[]) {
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/root/user", DATABASE_ROOT);
    FILE *file = fopen(filePath, "r");

    char line[BUFSIZ];
    while (fgets(line, BUFSIZ, file) != NULL) {
        if (strstr(line, user)) {
            fclose(file);
            return true;
        }
    }
    fclose(file);
    return false;
}

bool checkDB(char db[]) {
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/root/database", DATABASE_ROOT);
    FILE *file = fopen(filePath, "r");

    char line[BUFSIZ];
    while (fgets(line, BUFSIZ, file) != NULL) {
        if (strstr(line, db)) {
            fclose(file);
            return true;
        }
        // printf("%s", line);
    }
    fclose(file);
    return false;
}

bool checkPermission(char db[], char user[]) {
    if (!strcmp(user, "root")) return true;
    // ? get user
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/root/user", DATABASE_ROOT);
    FILE *file = fopen(filePath, "r");

    int userId = 0;
    char line[BUFSIZ];
    while (fgets(line, BUFSIZ, file) != NULL) {
        if (strstr(line, user)) {
            userId = atoi(line);
            break;
        }
    }
    fclose(file);

    // ? get db
    sprintf(filePath, "%s/root/database", DATABASE_ROOT);
    file = fopen(filePath, "r");

    int dbId = 0;
    while (fgets(line, BUFSIZ, file) != NULL) {
        if (strstr(line, db)) {
            dbId = atoi(line);
            break;
        }
    }
    fclose(file);

    // ? get user|db
    sprintf(filePath, "%s/root/database_permission", DATABASE_ROOT);
    char target[BUFSIZ];
    sprintf(target, "%d|%d|", userId, dbId);

    file = fopen(filePath, "r");
    while (fgets(line, BUFSIZ, file) != NULL) {
        if (strstr(line, target)) {
            fclose(file);
            return true;
        }
    }
    fclose(file);
    return false;
}

bool checkTable(char db[], char table[]) {
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/%s/%s", DATABASE_ROOT, db, table);

    if (fileOrDirectoryExists(filePath) == 2) return true;
    return false;
}

bool checkColumn(char db[], char table[], char column[]) {
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/%s/%s", DATABASE_ROOT, db, table);

    FILE *file = fopen(filePath, "r");
    char line[BUFSIZ];
    fgets(line, BUFSIZ, file);
    fclose(file);
    if (strstr(line, column)) return true;
    return false;
}

int getColumnCount(char db[], char table[]) {
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/%s/%s", DATABASE_ROOT, db, table);
    FILE *file = fopen(filePath, "r");

    char line[BUFSIZ];
    fgets(line, BUFSIZ, file);
    fclose(file);
    int attributeCount = 0;
    for (int i = 0; i < strlen(line); i++)
        if (line[i] == '|') attributeCount++;

    return attributeCount;
}

void insertInto(char db[], char table[], int columnsSize, char **columns) {
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/%s/%s", DATABASE_ROOT, db, table);
    FILE *file = fopen(filePath, "a");
    for (int j = 0; j <= columnsSize; j++) {
        fprintf(file, "%s|", columns[j]);
    }
    fprintf(file, "\n");
    fclose(file);
}

void dropDB(char db[]) {
    if (!strcmp(db, "root")) printf("cannot drop root database!\n");
    sprintf(cmd, "rm -r '%s/%s'", DATABASE_ROOT, db);
    system(cmd);

    // ? getting id & lineAt
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/root/database", DATABASE_ROOT);
    FILE *file = fopen(filePath, "r");

    char line[BUFSIZ];
    int atLine = 0, dbId = 0;
    while (fgets(line, BUFSIZ, file) != NULL) {
        atLine++;
        if (strstr(line, db)) {
            dbId = atoi(line);
            break;
        }
    }
    fclose(file);

    // ? remove database data at root/database
    sprintf(cmd, "sed '%dd' %s/root/database > temp.txt && mv temp.txt %s/root/database", atLine, DATABASE_ROOT, DATABASE_ROOT);
    system(cmd);

    // ? remove permission data at root/database_permission
}

void dropTable(char db[], char table[]) {
    if (!strcmp(db, "root")) printf("cannot drop root's table!\n");
    sprintf(cmd, "rm -r '%s/%s/%s'", DATABASE_ROOT, db, table);
    system(cmd);
}

void dropColumn(char db[], char table[], char column[]) {
    if (!strcmp(db, "root")) printf("cannot drop root's table!\n");
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/%s/%s", DATABASE_ROOT, db, table);

    FILE *file = fopen(filePath, "r");
    char line[BUFSIZ];
    fgets(line, BUFSIZ, file);

    char word[BUFSIZ];
    int columntAt, idx = 0, columnCount = 0;

    for (int i = 0; i < strlen(line); i++) {
        if (line[i] == '|') {
            columnCount++;
            if (strstr(word, column)) {
                columntAt = columnCount;
                break;
            }
            idx = 0;
        } else
            word[idx++] = line[i];
    }
    // printf("columm: %d\n", columntAt);

    sprintf(cmd, "awk 'BEGIN {FS=OFS=\"|\"} {for (i=1; i<=NF; i++) {if (i != n) printf \"%%s%%s\", $i, (i==NF?\"\\n\":OFS)}}' n=%d %s > output.txt; mv output.txt %s", columntAt, filePath, filePath);
    system(cmd);
}

void updateColumn(char db[], char table[], char column[], char value[]) {
    if (!strcmp(db, "root")) printf("cannot drop root's table!\n");
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/%s/%s", DATABASE_ROOT, db, table);

    sprintf(cmd, "sed 's/%s/%s/g' %s > output.txt; mv output.txt %s", column,
            value, filePath, filePath);
    system(cmd);
}

void selectAllQuery(char db[], char table[]) {
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/%s/%s", DATABASE_ROOT, db, table);

    FILE *file = fopen(filePath, "r");

    char line[BUFSIZ];
    while (fgets(line, BUFSIZ, file) != NULL) {
        printf("%s", line);
    }
    printf("\n");
    fclose(file);
}

void selectQuery(char db[], char table[], int columnsSize, char **columns) {
    char filePath[BUFSIZ] = {0};
    sprintf(filePath, "%s/%s/%s", DATABASE_ROOT, db, table);
    FILE *file = fopen(filePath, "r");

    for (int i = 0; i <= columnsSize; i++) printf("%s\\", columns[i]);

    char line[BUFSIZ], word[BUFSIZ];
    fgets(line, BUFSIZ, file);
    fclose(file);

    int columnsIndex[columnsSize], wordCount = 0, idxWord = 0;
    for (int i = 0; i <= columnsSize; i++) {
        for (int j = 0; j < strlen(line); j++) {
            if (line[j] == '|') {
                word[idxWord] = '\0';
                if (strstr(word, columns[i])) {
                    columnsIndex[i] = wordCount;
                    break;
                }
                wordCount++;
                idxWord = 0;
                memset(word, 0, BUFSIZ);
            } else {
                word[idxWord++] = line[j];
            }
        }
        wordCount = idxWord = 0;
    }

    sprintf(cmd, "awk -F'|' '{ printf \"");
    if (columnsSize > 1) {
        for (int i = 0; i < columnsSize; i++) sprintf(cmd, "%s%%s|", cmd);

        sprintf(cmd, "%s\\n\", ", cmd);

        for (int i = 0; i < columnsSize; i++)
            sprintf(cmd, "%s$%d, ", cmd, columnsIndex[i] + 1);

        sprintf(cmd, "%s$%d }' %s ", cmd, columnsIndex[columnsSize] + 1,
                filePath);
    } else {
        sprintf(cmd, "%s%%s|\\n\", $%d }' %s", cmd,
                columnsIndex[0] + 1, filePath);
    }
    system(cmd);
}

void dumpDB(char db_name[]) {
    char folderPath[BUFFER_SIZE] = {0};
    sprintf(folderPath, "%s/%s", DATABASE_ROOT, db_name);

    DIR *directory;
    struct dirent *file;

    // Open the directory
    directory = opendir(folderPath);

    if (directory) {
        // Read the files in the directory
        while ((file = readdir(directory)) != NULL) {
            // Skip the current and parent directory entries
            if (strcmp(file->d_name, ".") == 0 || strcmp(file->d_name, "..") == 0) {
                continue;
            }
            
            printf("%s\n", file->d_name);

            FILE *fileContent;
            char line[BUFFER_SIZE], dumpLine[BUFFER_SIZE], filePath[BUFSIZ];
            sprintf(filePath, "%s/%s", folderPath, file->d_name);
            fopen(filePath, "r");
            int i = 0, lineLength;

            if (fgets(line, BUFFER_SIZE, fileContent) != NULL) {
                fprintf(fileContent, "DROP TABLE %s;", file->d_name);
                sprintf(dumpLine, "CREATE TABLE %s (", file->d_name);

                while(line[i] != '\0') {
                    if (line[i] == ':')
                        strcat(dumpLine, " ");
                    else if (line[i] == '|') {
                        if (i != (strlen(line) - 1))
                            strcat(dumpLine, ",");
                    }
                    else {
                        lineLength = strlen(dumpLine);
                        dumpLine[lineLength] = line[i];
                    }
                    
                    i++;
                }

                strcat(dumpLine, ");");
                fprintf(fileContent, "%s", dumpLine);
            }

            while (fgets(line, BUFFER_SIZE, fileContent) != NULL) {
                fprintf(fileContent, "INSERT INTO %s (", file->d_name);

                while(line[i] != '\0') {
                    if (line[i] == ':')
                        strcat(dumpLine, " ");
                    else if (line[i] == '|') {
                        if (i != (strlen(line) - 1))
                            strcat(dumpLine, ",");
                    }
                    else {
                        lineLength = strlen(dumpLine);
                        dumpLine[lineLength] = line[i];
                    }
                    
                    i++;
                }

                strcat(dumpLine, ");");
                fprintf(fileContent, "%s", dumpLine);
            }

            fclose(fileContent);
        }

        // Close the directory
        closedir(directory);
    } else {
        printf("Unable to open the directory.\n");
    }
}

int main() {
    createDatabaseSystem();

    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[BUFFER_SIZE] = {0};

    // Create a socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Set socket options
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Bind the socket to a specific port
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    // Listen for incoming connections
    if (listen(server_fd, 3) < 0) {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }

    // Accept multiple connections and handle communication
    while (1) {
        // Accept a new connection
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
            perror("accept failed");
            exit(EXIT_FAILURE);
        }

        printf("New client connected.\n");

        // ! Read data from the client
        char input[BUFSIZ], user[BUFSIZ], pass[BUFSIZ];
        int bytesRead = read(new_socket, input, BUFSIZ);
        if (bytesRead < 0) {
            perror("read failed");
            exit(EXIT_FAILURE);
        } else if (bytesRead == 0) {
            printf("Client disconnected.\n");
            break;
        }

        input[bytesRead] = '\0';  // Add null terminator

        // Remove newline characters from the received string
        size_t newlinePos = strcspn(input, "\r\n");
        input[newlinePos] = '\0';

        bool userExist = false;
        if (!strncmp(input, "root", 4)) {
            strcpy(user, "root");
            userExist = true;
        } else {
            char word[3][BUFSIZ];
            int wordIndex = 0, charIndex = 0;

            // seperating user and pass
            for (int i = 0; input[i] != '\0'; i++) {
                if (input[i] == ' ') {
                    word[wordIndex][charIndex] = '\0';
                    wordIndex++;
                    charIndex = 0;
                } else
                    word[wordIndex][charIndex++] = input[i];
            }
            word[wordIndex][charIndex] = '\0';

            strcpy(user, word[0]);
            strcpy(pass, word[1]);

            printf("%s:%s\n", user, pass);
            userExist = checkUserPass(user, pass);
        }

        if (userExist == false) {
            printf("username or password is incorrect\n");

        } else {
            // Communication loop
            char use[BUFSIZ] = "none";
            while (1) {
                // Read data from the client
                int bytesRead = read(new_socket, buffer, BUFFER_SIZE);
                if (bytesRead < 0) {
                    perror("read failed");
                    exit(EXIT_FAILURE);
                } else if (bytesRead == 0) {
                    printf("%s disconnected.\n", user);
                    break;
                }

                printf("%s says: %s\n", user, buffer);

                // ? create user
                if (strstr(buffer, "CREATE USER ") &&
                    strstr(buffer, "IDENTIFIED BY ")) {
                    if (!strcmp(user, "root")) {
                        char createUser[BUFSIZ], createPass[BUFSIZ];
                        int index = 12, idx = 0;
                        while (buffer[index] != ' ') {
                            createUser[idx++] = buffer[index++];
                        }
                        createUser[idx] = '\0';
                        strstr(buffer, "IDENTIFIED BY ");
                        index += 15, idx = 0;
                        while (buffer[index] != ';') {
                            createPass[idx++] = buffer[index++];
                        }
                        createPass[idx] = '\0';
                        addUser(createUser, createPass);
                        printf("user created!\n");
                    } else {
                        printf("non root cannot create user!\n");
                    }
                }

                // ? create database
                else if (strstr(buffer, "CREATE DATABASE ")) {
                    char db[BUFSIZ];
                    int index = 16, idx = 0;
                    while (buffer[index] != ';') {
                        db[idx++] = buffer[index++];
                    }
                    db[idx] = '\0';
                    addDB(db);
                    createDatabase(db);
                    printf("database created!\n");
                    if (strcmp(user, "root")) {
                        addDB_permission(db, user);
                        printf("permission added!\n");
                    }
                }

                // ? grant permission
                else if (!strcmp(user, "root") &&
                         strstr(buffer, "GRANT PERMISSION ") &&
                         strstr(buffer, "INTO ")) {
                    char createUser[BUFSIZ], db[BUFSIZ];
                    int index = 17, idx = 0;
                    while (buffer[index] != ' ') {
                        db[idx++] = buffer[index++];
                    }
                    db[idx] = '\0';
                    strstr(buffer, "INTO ");
                    index += 6, idx = 0;
                    while (buffer[index] != ';') {
                        createUser[idx++] = buffer[index++];
                    }
                    createUser[idx] = '\0';
                    if (checkDB(db) == false)
                        printf("database %s not found!\n", db);
                    if (checkUser(createUser) == false)
                        printf("user %s not found!\n", createUser);
                    else {
                        addDB_permission(db, createUser);
                        printf("permission added!\n");
                    }
                }

                // ? use
                else if (strstr(buffer, "USE ")) {
                    char db[BUFSIZ];
                    int index = 4, idx = 0;
                    while (buffer[index] != ';') {
                        db[idx++] = buffer[index++];
                    }
                    db[idx] = '\0';

                    if (checkDB(db) == false)
                        printf("database %s not found!\n", db);
                    if (checkPermission(db, user) == true) {
                        strcpy(use, db);
                        printf("connected to %s\n", db);
                    } else
                        printf("user %s have no privilege to %s\n ", user, db);
                }

                // ? create table
                else if (strstr(buffer, "CREATE TABLE ")) {
                    if (!strcmp(use, "none")) {
                        printf("not connected to a database!\n");
                    } else {
                        char table[BUFSIZ], attributes[BUFSIZ];
                        int index = 13, idx = 0;
                        while (buffer[index] != ' ') {
                            table[idx++] = buffer[index++];
                        }
                        table[idx] = '\0';

                        index += 2, idx = 0;
                        while (buffer[index] != ')') {
                            attributes[idx++] = buffer[index++];
                        }
                        attributes[idx] = '\0';

                        int wordCount;
                        char **words;

                        splitString(attributes, &words, &wordCount);
                        createTable(use, table, wordCount, words);
                        printf("table created!\n");
                    }
                }

                // ? drop
                else if (strstr(buffer, "DROP ")) {
                    if (!strcmp(use, "none")) {
                        printf("not connected to a database!\n");
                    } else {
                        if (!strncmp(buffer, "DROP DATABASE ", 14)) {
                            char db[BUFSIZ];
                            int index = 14, idx = 0;
                            while (buffer[index] != ';') {
                                db[idx++] = buffer[index++];
                            }
                            db[idx] = '\0';

                            if (checkDB(db) == false || strcmp(use, db)) {
                                if (checkDB(db) == false)
                                    printf("database %s not found!\n", db);
                                if (strcmp(use, db))
                                    printf("not connected to %s!\n", db);
                            } else {
                                dropDB(db);
                                strcpy(use, "none");
                                printf("database dropped!\n");
                            }

                        } else if (!strncmp(buffer, "DROP TABLE ", 11)) {
                            char table[BUFSIZ];
                            int index = 11, idx = 0;
                            while (buffer[index] != ';') {
                                table[idx++] = buffer[index++];
                            }
                            table[idx] = '\0';

                            if (checkTable(use, table) == false)
                                printf("table %s in %s not found!\n", table, use);
                            else {
                                dropTable(use, table);
                                printf("table dropped!\n");
                            }
                        } else if (!strncmp(buffer, "DROP COLUMN ", 12) &&
                                   strstr(buffer, "FROM ")) {
                            char table[BUFSIZ], column[BUFSIZ];
                            int index = 12, idx = 0;
                            while (buffer[index] != ' ') {
                                column[idx++] = buffer[index++];
                            }
                            column[idx] = '\0';
                            strstr(buffer, "FROM ");
                            index += 6, idx = 0;
                            while (buffer[index] != ';') {
                                table[idx++] = buffer[index++];
                            }
                            table[idx] = '\0';

                            if (checkTable(use, table) == false ||
                                checkColumn(use, table, column) == false) {
                                if (checkTable(use, table) == false)
                                    printf("table %s in %s not found!\n", table,
                                           use);
                                else {
                                    if (checkColumn(use, table, column) == false)
                                        printf("column %s in %s not found!\n", column, table);
                                }
                            } else {
                                dropColumn(use, table, column);
                                printf("column dropped!\n");
                            }
                        }
                    }
                }

                // ? insert into
                else if (strstr(buffer, "INSERT INTO ")) {
                    if (!strcmp(use, "none")) {
                        printf("not connected to a database!\n");
                    } else {
                        char table[BUFSIZ], attributes[BUFSIZ];
                        int index = 12, idx = 0;
                        while (buffer[index] != ' ') {
                            table[idx++] = buffer[index++];
                        }
                        table[idx] = '\0';

                        index += 2, idx = 0;
                        while (buffer[index] != ')') {
                            attributes[idx++] = buffer[index++];
                        }
                        attributes[idx] = '\0';

                        bool aman = true;
                        if (checkTable(use, table) == false) {
                            printf("table %s in %s not found!\n", table, use);
                            aman = false;
                        }

                        if (aman) {
                            int columnCount;
                            char **columns;
                            splitString(attributes, &columns, &columnCount);

                            if (getColumnCount(use, table) != columnCount + 1) {
                                aman = false;
                                printf(
                                    "number of data inserted is not "
                                    "correct!\n");
                            }

                            if (aman) {
                                insertInto(use, table, columnCount, columns);
                                printf("data inserted!\n");
                            }
                            freeWordsArray(columns);
                        }
                    }

                }

                // ? update
                else if (strstr(buffer, "UPDATE ")) {
                    if (!strcmp(use, "none")) {
                        printf("not connected to a database!\n");
                    } else {
                        char table[BUFSIZ], column[BUFSIZ], value[BUFSIZ];
                        int index = 7, idx = 0;
                        while (buffer[index] != ' ') {
                            table[idx++] = buffer[index++];
                        }
                        table[idx] = '\0';

                        strstr(buffer, "SET ");
                        index += 5, idx = 0;
                        while (buffer[index] != '=') {
                            column[idx++] = buffer[index++];
                        }
                        column[idx] = '\0';
                        index++, idx = 0;
                        ;
                        while (buffer[index] != ';') {
                            value[idx++] = buffer[index++];
                        }
                        value[idx] = '\0';

                        bool aman = true;
                        if (checkTable(use, table) == false) {
                            printf("table %s in %s not found!\n", table, use);
                            aman = false;
                        }

                        if (aman) {
                            if (checkColumn(use, table, column) == false) {
                                printf("column %s in %s not found!\n", column,
                                       table);
                                aman = false;
                            }
                        }
                        printf("value: %s\n", value);
                        if (aman) {
                            updateColumn(use, table, column, value);
                            printf("table altered!\n");
                        }
                    }

                }

                // ? select
                else if (strstr(buffer, "SELECT ") && strstr(buffer, "FROM ")) {
                    if (!strcmp(use, "none")) {
                        printf("not connected to a database!\n");
                    } else {
                        char *e;
                        int indexStop;
                        e = strstr(buffer, "FROM");
                        indexStop = (int)(e - buffer);

                        char columnsInput[BUFSIZ], table[BUFSIZ];
                        int index = 7, idx = 0;
                        while (index < indexStop) {
                            columnsInput[idx++] = buffer[index++];
                        }
                        columnsInput[idx] = '\0';

                        strstr(buffer, "FROM ");
                        index += 5, idx = 0;
                        while (buffer[index] != ';') {
                            table[idx++] = buffer[index++];
                        }
                        table[idx] = '\0';

                        printf("columns: %s\n", columnsInput);
                        printf("table: %s\n", table);

                        bool aman = true;
                        if (checkTable(use, table) == false) {
                            printf("table %s in %s not found!\n", table, use);
                            aman = false;
                        }

                        if (aman) {
                            if (!strncmp(columnsInput, "*", 1))
                                selectAllQuery(use, table);
                            else {
                                int columnCount;
                                char **columns;
                                splitString(columnsInput, &columns,
                                            &columnCount);
                                if (getColumnCount(use, table) < columnCount) {
                                    aman = false;
                                    printf(
                                        "number of data inserted is not "
                                        "correct!\n");
                                }

                                if (aman) {
                                    for (int i = 0; i <= columnCount; i++) {
                                        if (checkColumn(use, table,
                                                        columns[i]) == false) {
                                            aman = false;
                                            printf(
                                                "column %s in %s not found!\n",
                                                columns[i], table);
                                            break;
                                        }
                                    }
                                }

                                if (aman) {
                                    selectQuery(use, table, columnCount,
                                                columns);
                                }
                            }
                        }
                    }
                }

                else if (strstr(buffer, "DUMP ")) {
                    char db[BUFFER_SIZE];
                    int index = 5, idx = 0;
                    while (buffer[index] != ';') {
                        db[idx++] = buffer[index++];
                    }
                    db[idx] = '\0';

                    if (checkDB(db) == false)
                        printf("database %s not found!\n", db);
                    if (checkPermission(db, user) == true) {
                        strcpy(use, db);
                        printf("connected to %s\n", db);
                        dumpDB(db);
                    } else
                        printf("user %s have no privilege to %s\n ", user, db);
                }

                // Check if the client wants to exit
                else if (strcmp(buffer, "exit") == 0) {
                    printf("%s disconnected.\n", user);
                    break;
                }

                // Check for unknown command buffer
                else
                    printf("command not found!\n");

                // Clear the buffer and response
                memset(buffer, 0, BUFFER_SIZE);
            }
        }

        // Close the connection socket
        close(new_socket);
    }

    // Close the server socket
    close(server_fd);

    return 0;
}