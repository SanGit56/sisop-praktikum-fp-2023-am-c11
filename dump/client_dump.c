#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define DATABASE_ROOT "../databases"
#define PORT 8080
#define BUFFER_SIZE 1024

int main(int argc, char const *argv[])
{
    bool isRoot = false;
    if (argc == 6 && !strcmp(argv[1], "-u") && !strcmp(argv[3], "-p")) {
        printf("login pake user\n");

        if (!strcmp(argv[2], "root") && !strcmp(argv[4], "root")) {
            printf("login pake sudo\n");
            isRoot = true;
        }
    } else {
        fprintf(stderr, "Error, login command '-u [username] -p [password] [database_name]'\n");
        exit(EXIT_FAILURE);
    }

    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char message[BUFFER_SIZE] = {0};
    char buffer[BUFFER_SIZE] = {0};

    // Create a socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    // Connect to the server
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    // send the user to database.c
    char user[BUFSIZ];
    if (isRoot) {
        sprintf(message, "root ");
        strcpy(user, "root");
    } else {
        sprintf(message, "%s %s ", argv[2], argv[4]);
        strcpy(user, argv[2]);
    }
    send(sock, message, strlen(message), 0);

    // Send message to the server
    sprintf(message, "DUMP %s;", argv[5]);
    send(sock, message, strlen(message), 0);
    printf("Dumpling database %s.\n", argv[5]);

    // Clear the buffer and message
    memset(message, 0, BUFFER_SIZE);

    // Close the socket
    close(sock);

    return 0;
}