# Build the server image
FROM gcc:latest as server-builder

COPY database/database.c /app/database.c
WORKDIR /app

RUN gcc -o database database.c

# Build the client image
FROM gcc:latest as client-builder

COPY client/client.c /app/client.c
WORKDIR /app

RUN gcc -o client client.c

# Final image
FROM ubuntu:latest

COPY --from=server-builder /app/database /app/database
COPY --from=client-builder /app/client /app/client

# Set the working directory
WORKDIR /app

# Expose any necessary ports
EXPOSE 8080
