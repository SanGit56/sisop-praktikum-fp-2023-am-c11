## Anggota Kelompok

| NRP        | NAMA                        |
| ---------- | --------------------------- |
| 5025211149 | Irsyad Fikriansyah Ramadhan |
| 5025211244 | Ligar Arsa Arnata           |
| 5025211166 | Radhiyan Muhammad Hisan     |

## Kelengkapan

| Daftar                     | Status   |
| -------------------------- | -------- |
| Autentikasi                | DONE     |
| Authorisasi                | DONE     |
| Data Definition Language   | DONE     |
| Data Manipulation Language | PARTIAL  |
| Logging                    | DONE     |
| Reliability                | PARTIAL  |
| Tambahan                   |          |
| Error Handling             | DONE     |
| Containerization           | DONE     |

## Penjelasan

### Autentikasi

Proses Autentikasi yang dilakukan pada code database.c adalah sebagai berikut :

![Gambar 1](gambar/fp1.png)

- Add User
	Fungsi addUser digunakan untuk menambahkan seorang pengguna ke dalam file yang menyimpan informasi pengguna. Fungsi ini menerima dua parameter, yaitu user yang merupakan nama pengguna baru dan pass yang merupakan kata sandi pengguna baru. Fungsi ini menggunakan fungsi sprintf untuk membentuk perintah yang akan menambahkan baris baru ke file "root/user" dengan informasi pengguna baru. Perintah ini menggunakan perintah echo untuk menambahkan baris baru ke file, dengan format baris yang ditambahkan adalah "<jumlah_baris>|<user>|<pass>|". Jumlah baris dihitung menggunakan wc -l untuk menghitung jumlah baris dalam file. Perintah tersebut akan dijalankan menggunakan fungsi system, dengan path file "root/user" disimpan dalam variabel cmd.

### Otorisasi

Proses Otorisasi yang dilakukan pada code database.c adalah sebagai berikut :

![Gambar 2](gambar/fp2.png)

- Add Database
	Fungsi addDB digunakan untuk menambahkan sebuah database ke dalam file yang menyimpan informasi database. Fungsi ini menerima satu parameter, yaitu dbName yang merupakan nama database baru. Fungsi ini menggunakan fungsi sprintf untuk membentuk perintah yang akan menambahkan baris baru ke file "root/database" dengan informasi database baru. Perintah ini menggunakan perintah echo untuk menambahkan baris baru ke file, dengan format baris yang ditambahkan adalah "<jumlah_baris>|<dbName>|". Jumlah baris dihitung menggunakan wc -l untuk menghitung jumlah baris dalam file. Perintah tersebut akan dijalankan menggunakan fungsi system, dengan path file "root/database" disimpan dalam variabel cmd.
	
- Add Database Permission
	Fungsi addDB_permission digunakan untuk menambahkan izin akses sebuah pengguna terhadap sebuah database ke dalam file yang menyimpan informasi izin akses database. Fungsi ini menerima dua parameter, yaitu dbName yang merupakan nama database dan user yang merupakan nama pengguna. Langkah yang dilakukan pada fungsi ini adalah sebagai berikut :

	1. Fungsi menggunakan fungsi sprintf untuk membentuk path file pengguna yang akan dibaca, yaitu "%s/root/user", dengan DATABASE_ROOT sebagai direktori root database. Path tersebut disimpan dalam variabel filePath.

	2. Fungsi membuka file pengguna dengan mode "r" (read) menggunakan fungsi fopen, dan file tersebut disimpan dalam variabel file.

	3. Fungsi menggunakan perulangan while dan fungsi fgets untuk membaca setiap baris dari file pengguna. Setiap baris yang dibaca akan diperiksa apakah mengandung nama pengguna yang diberikan. Jika ditemukan baris yang mengandung nama pengguna, perulangan dihentikan dengan menggunakan break, dan variabel userId akan menyimpan nomor pengguna yang sedang diperiksa.

	4. Setelah selesai membaca file pengguna, file ditutup menggunakan fungsi fclose.

	5. Fungsi menggunakan fungsi sprintf untuk membentuk path file database yang akan dibaca, yaitu "%s/root/database", dengan DATABASE_ROOT sebagai direktori root database. Path tersebut disimpan kembali dalam variabel filePath.

	6. Fungsi membuka file database dengan mode "r" (read) menggunakan fungsi fopen, dan file tersebut disimpan kembali dalam variabel file.
	
	7. Fungsi menggunakan perulangan while dan fungsi fgets untuk membaca setiap baris dari file database. Setiap baris yang dibaca akan diperiksa apakah mengandung nama database yang diberikan. Jika ditemukan baris yang mengandung nama database, perulangan dihentikan dengan menggunakan break, dan variabel dbID akan menyimpan nomor database yang sedang diperiksa.

	8. Setelah selesai membaca file database, file ditutup menggunakan fungsi fclose.

	Fungsi menggunakan fungsi sprintf untuk membentuk perintah yang akan menambahkan baris baru ke file "root/database_permission" dengan informasi izin akses database. Perintah ini menggunakan perintah echo untuk menambahkan baris baru ke file, dengan format baris yang ditambahkan adalah "<userId>|<dbID>|". Perintah tersebut akan dijalankan menggunakan fungsi system, dengan path file "root/database_permission" disimpan dalam variabel cmd.


### Data Definition Language

Data Definition Language yang dapat dilakukan pada code database.c meliputi :

![Gambar 3](gambar/fp3.png)

- Create Database
	Fungsi createDatabase merupakan sebuah fungsi dalam bahasa pemrograman yang bertujuan untuk membuat sebuah database baru. Fungsi ini menerima parameter databaseName yang merupakan nama dari database yang ingin dibuat. Langkah yang dilakukan pada fungsi ini adalah sebagai berikut :
	
	1. Mendeklarasikan variabel lokal filePath dengan tipe data char dan ukuran BUFSIZ. Variabel ini digunakan untuk menyimpan path (lokasi) dari database yang akan dibuat.

	2. Menggunakan fungsi sprintf, variabel filePath diisi dengan nilai yang dihasilkan dari format string "%s/%s", dimana DATABASE_ROOT adalah direktori root (akar) dari database dan databaseName 				adalah nama dari database yang akan dibuat. Dengan demikian, nilai dari variabel filePath akan berisi path lengkap dari database yang akan dibuat.

	3. Mencoba membuka direktori yang memiliki path yang telah ditentukan oleh filePath menggunakan fungsi opendir. Jika direktori tersebut tidak dapat dibuka (NULL), berarti database belum ada.

	4. Dalam kondisi jika direktori database tidak ada, fungsi akan menjalankan perintah untuk membuat direktori tersebut menggunakan fungsi system. Fungsi sprintf juga digunakan untuk membentuk perintah dengan format string mkdir -p '%s', dimana %s akan diisi dengan nilai dari filePath. Perintah mkdir -p digunakan untuk membuat direktori secara rekursif, termasuk membuat direktori parent yang belum ada.

	5. Jika proses pembuatan direktori berhasil dilakukan, fungsi createDatabase akan mengembalikan nilai 1, menandakan bahwa database telah berhasil dibuat.

	6. Jika direktori database sudah ada, fungsi createDatabase akan mengembalikan nilai 0, menandakan bahwa database dengan nama yang sama sudah ada.

	Fungsi createDatabase ini menggunakan beberapa fungsi bawaan dari bahasa pemrograman C, seperti sprintf, system, dan opendir. Fungsi sprintf digunakan untuk melakukan format string, system digunakan untuk menjalankan perintah sistem, dan opendir digunakan untuk membuka direktori.

![Gambar 4](gambar/fp4.png)

- Create Table
	Fungsi createTable digunakan untuk membuat sebuah tabel di dalam sebuah database. Fungsi ini menerima beberapa parameter seperti database yaitu Nama dari database tempat tabel akan dibuat, table yaitu Nama tabel yang akan dibuat, attributeAmount yaitu Jumlah atribut yang akan dimasukkan ke dalam tabel, attributes yaitu Array dua dimensi yang berisi atribut-atribut yang akan dimasukkan ke dalam tabel. Setiap atribut terdiri dari dua elemen, yaitu nama atribut dan tipe atribut. Langkah yang dilakukan pada fungsi ini adalah sebagai berikut :

	1. Mendeklarasikan variabel lokal filePath dengan tipe data char dan ukuran BUFSIZ. Variabel ini digunakan untuk menyimpan path (lokasi) dari tabel yang akan dibuat.

	2. Menggunakan fungsi sprintf, variabel filePath diisi dengan nilai yang dihasilkan dari format string "%s/%s/%s", dimana DATABASE_ROOT adalah direktori root (akar) dari database, database adalah nama dari database yang akan dituju, dan table adalah nama tabel yang akan dibuat. Dengan demikian, nilai dari variabel filePath akan berisi path lengkap dari tabel yang akan dibuat.

	3. Memanggil fungsi fileOrDirectoryExists dengan parameter filePath untuk memeriksa apakah tabel sudah ada atau belum. Nilai yang dikembalikan oleh fungsi ini akan disimpan dalam variabel result.

	4. Dalam kondisi jika tabel belum ada (nilai result adalah 0), fungsi akan menjalankan perintah touch menggunakan fungsi system. Fungsi sprintf juga digunakan untuk membentuk perintah dengan format string touch '%s', dimana %s akan diisi dengan nilai dari filePath. Perintah touch digunakan untuk membuat file kosong dengan nama dan path yang telah ditentukan.

	5. Setelah membuat file tabel kosong, fungsi akan membuka file tersebut menggunakan fungsi fopen dengan mode "w" (write). File tersebut akan disimpan dalam variabel tableFile.
	
	6. Selanjutnya, fungsi akan melakukan perulangan menggunakan variabel j dari 0 hingga attributeAmount - 1 dengan penambahan 2 setiap iterasi. Perulangan ini akan digunakan untuk menulis atribut-atribut ke dalam file tabel. Pada setiap iterasi, fungsi fprintf akan digunakan untuk menulis nama atribut dan tipe atribut ke dalam file tabel dengan format <nama_atribut>:<tipe_atribut>|.

	7. Setelah selesai menulis atribut-atribut ke dalam file tabel, fungsi akan menulis karakter baru (\n) ke dalam file menggunakan fungsi fprintf. Hal ini bertujuan untuk membuat baris baru setelah penulisan atribut-atribut.

	8. Terakhir, file tabel akan ditutup menggunakan fungsi fclose.

	Dalam kondisi jika tabel sudah ada (nilai result adalah 2), fungsi akan mencetak pesan "table <nama_tabel> on <nama_database> is already exist!" menggunakan fungsi printf, yang memberitahukan bahwa tabel dengan nama yang sama sudah ada dalam database yang dituju.Fungsi createTable menggunakan beberapa fungsi bawaan dari bahasa pemrograman C, seperti sprintf, system, fopen, fprintf, fclose, dan printf.
	
![Gambar 5](gambar/fp5.png)
	
- Drop Database
	Fungsi dropDB digunakan untuk menghapus sebuah database. Fungsi ini menerima parameter db yang merupakan nama database yang akan dihapus. Langkah yang dilakukan pada fungsi ini adalah sebagai berikut :
	
	1. Pertama, fungsi memeriksa apakah nama database yang diberikan adalah "root" atau bukan. Jika nama database adalah "root", fungsi akan mencetak pesan "cannot drop root database!" menggunakan fungsi printf dan menghentikan proses penghapusan.

	2. Fungsi menggunakan fungsi sprintf untuk membentuk perintah yang akan menjalankan perintah rm -r untuk menghapus direktori database beserta isinya. Perintah ini akan dijalankan menggunakan fungsi system, dengan path direktori database yang akan dihapus disimpan dalam variabel cmd.

	3. Selanjutnya, fungsi membaca file "root/database" untuk mendapatkan informasi mengenai database yang akan dihapus. Fungsi sprintf digunakan untuk membentuk path file yang akan dibaca, yaitu "%s/root/database", dengan DATABASE_ROOT sebagai direktori root database.

	4. Fungsi membuka file tersebut dengan mode "r" (read) menggunakan fungsi fopen, dan file tersebut disimpan dalam variabel file.
	
	5. Selama file tersebut belum habis dibaca, fungsi menggunakan fungsi fgets untuk membaca setiap baris dari file. Variabel line digunakan untuk menyimpan isi baris yang sedang dibaca. Setiap baris yang dibaca akan diperiksa apakah mengandung nama database yang akan dihapus. Jika ditemukan baris yang mengandung nama database, variabel dbId akan diisi dengan nilai yang diconversi dari baris tersebut menggunakan fungsi atoi, dan loop akan dihentikan dengan menggunakan break.

	6. Setelah selesai membaca file, file ditutup menggunakan fungsi fclose.

	7. Selanjutnya, fungsi menggunakan fungsi sprintf untuk membentuk perintah yang akan menghapus baris yang mengandung informasi database yang dihapus dari file "root/database". Perintah ini menggunakan perintah sed untuk menghapus baris tertentu dari file, yaitu baris ke-atLine. Perintah tersebut akan menghasilkan file sementara "temp.txt", yang kemudian akan diganti namanya menjadi file "root/database" yang telah diubah menggunakan perintah mv. Perintah ini akan dijalankan menggunakan fungsi system, dengan path direktori root database disimpan dalam variabel cmd.

	8. Terakhir, fungsi akan menghapus data izin (permission data) yang terkait dengan database yang dihapus. 

	Fungsi dropDB menggunakan beberapa fungsi bawaan dari bahasa pemrograman C, seperti strcmp, sprintf, system, fgets, atoi, fopen, fclose, dan printf.
	
![Gambar 6](gambar/fp6.png)	

- Drop Table
	Fungsi dropTable digunakan untuk menghapus sebuah tabel dari dalam sebuah database. Fungsi ini menerima dua parameter, yaitu db yang merupakan nama database dan table yang merupakan nama tabel yang akan dihapus. Langkah yang dilakukan pada fungsi ini adalah sebagai berikut :

	1. Pertama, fungsi memeriksa apakah nama database yang diberikan adalah "root" atau bukan. Jika nama database adalah "root", fungsi akan mencetak pesan "cannot drop root's table!" menggunakan fungsi printf dan menghentikan proses penghapusan.

	2. Fungsi menggunakan fungsi sprintf untuk membentuk perintah yang akan menjalankan perintah rm -r untuk menghapus direktori tabel beserta isinya. Perintah ini akan dijalankan menggunakan fungsi system, dengan path direktori tabel yang akan dihapus disimpan dalam variabel cmd.

	Serta juga terdapat Fungsi dropColumn digunakan untuk menghapus sebuah kolom dari sebuah tabel dalam sebuah database. Fungsi ini menerima tiga parameter, yaitu db yang merupakan nama database, table yang merupakan nama tabel, dan column yang merupakan nama kolom yang akan dihapus.
	
### Data Manipulation Language

Data Manipulation Language yang dapat dilakukan pada code database.c meliputi :

![Gambar 7](gambar/fp7.png)

- Insert 
	Fungsi insertInto digunakan untuk melakukan operasi INSERT pada sebuah tabel dalam database. Fungsi ini menerima beberapa parameter, yaitu db yang merupakan nama database, table yang merupakan nama tabel, columnsSize yang merupakan jumlah kolom, dan columns yang merupakan array of string yang berisi nilai-nilai kolom yang akan diinsert. Langkah yang dilakukan pada fungsi ini adalah sebagai berikut : 

	1. Fungsi menggunakan fungsi sprintf untuk membentuk path file tabel yang akan dibuka, yaitu "%s/%s/%s", dengan DATABASE_ROOT sebagai direktori root database, db sebagai nama database, dan table sebagai nama tabel. Path tersebut disimpan dalam variabel filePath.

	2. Fungsi membuka file tabel dengan mode "a" (append) menggunakan fungsi fopen, dan file tersebut disimpan dalam variabel file. Mode "a" digunakan untuk menambahkan data ke akhir file, sehingga data baru akan ditambahkan setiap kali fungsi ini dipanggil.

	3. Fungsi menggunakan perulangan for untuk memproses setiap nilai kolom yang akan diinsert. Perulangan dimulai dari 0 hingga columnsSize (termasuk indeks terakhir).

	4. Pada setiap iterasi perulangan, fungsi menggunakan fprintf untuk menulis nilai kolom ke file dengan format <nilai_kolom>|. %s digunakan untuk menggantikan placeholder dengan nilai kolom yang sesuai dari array columns.

	5. Setelah semua nilai kolom ditulis, fungsi menggunakan fprintf untuk menulis karakter newline (\n) ke file, sehingga setiap baris akan berisi satu data yang diinsert.

	6. Terakhir, file ditutup menggunakan fungsi fclose untuk menyimpan perubahan yang dilakukan.

	Dengan langkah yang telah dijelaskan, fungsi insertInto akan menambahkan data baru ke file tabel dengan memasukkan nilai-nilai kolom yang diberikan dalam parameter ke akhir baris baru.

![Gambar 8](gambar/fp8.png)	

- Update 
	Fungsi updateColumn digunakan untuk mengubah nilai sebuah kolom dalam sebuah tabel dalam database. Fungsi ini menerima beberapa parameter, yaitu db yang merupakan nama database, table yang merupakan nama tabel, column yang merupakan nama kolom yang akan diubah, dan value yang merupakan nilai baru yang akan diisikan ke kolom tersebut. Langkah yang dilakukan pada fungsi ini adalah sebagai berikut :
	
	1. Fungsi menggunakan kondisi if untuk memeriksa apakah nama database yang diberikan adalah "root". Jika benar, maka fungsi mencetak pesan "cannot drop root's table!" dan fungsi berhenti.

	2. Fungsi menggunakan fungsi sprintf untuk membentuk path file tabel yang akan dibaca dan diubah, yaitu "%s/%s/%s", dengan DATABASE_ROOT sebagai direktori root database, db sebagai nama database, dan table sebagai nama tabel. Path tersebut disimpan dalam variabel filePath.

	3. Fungsi menggunakan fungsi sprintf untuk membentuk perintah yang akan menggantikan nilai kolom dalam file tabel. Perintah ini menggunakan perintah sed untuk mencari dan menggantikan nilai kolom yang sesuai. Format perintahnya adalah sed 's/<column>/<value>/g' <filePath> > output.txt; mv output.txt <filePath>. Perintah tersebut akan dijalankan menggunakan fungsi system, dengan path file tabel disimpan dalam variabel cmd.

	4. Dalam perintah sed, <column> akan digantikan dengan nilai parameter column, dan <value> akan digantikan dengan nilai parameter value. Kode 'g' menandakan bahwa semua kemunculan kolom yang sesuai akan digantikan, bukan hanya yang pertama ditemukan.

	5. Perintah sed akan menghasilkan output yang ditulis ke file output.txt. Kemudian, file output.txt akan dipindahkan (rename) menjadi file tabel yang asli menggunakan perintah mv.
	
	Dengan langkah yang telah dijelaskan, fungsi updateColumn akan melakukan penggantian nilai kolom yang sesuai dengan nilai baru yang diberikan dalam file tabel yang bersangkutan.
	
![Gambar 9](gambar/fp9.png)

- Select
	Fungsi selectAllQuery digunakan untuk melakukan query SELECT * pada sebuah tabel dalam database. Fungsi ini menerima dua parameter, yaitu db yang merupakan nama database, dan table yang merupakan nama tabel. Langkah yang dilakukan pada fungsi ini adalah sebagai berikut :

	1. Fungsi menggunakan fungsi sprintf untuk membentuk path file tabel yang akan dibaca, yaitu "%s/%s/%s", dengan DATABASE_ROOT sebagai direktori root database, db sebagai nama database, dan table sebagai nama tabel. Path tersebut disimpan dalam variabel filePath.

	2. Fungsi membuka file tabel dengan mode "r" (read) menggunakan fungsi fopen, dan file tersebut disimpan dalam variabel file.

	3. Fungsi menggunakan perulangan while dan fungsi fgets untuk membaca setiap baris dari file tabel. Setiap baris yang dibaca akan dicetak menggunakan printf.

	4. Setelah selesai membaca file tabel, file ditutup menggunakan fungsi fclose.

	Dengan langkah yang telah dijelaskan, fungsi selectAllQuery akan mencetak semua baris yang ada dalam tabel ke layar.
	
![Gambar 10](gambar/fp10.png)
	
	Fungsi selectQuery digunakan untuk melakukan query SELECT dengan kolom-kolom tertentu pada sebuah tabel dalam database. Fungsi ini menerima beberapa parameter, yaitu db yang merupakan nama database, table yang merupakan nama tabel, columnsSize yang merupakan jumlah kolom yang akan dipilih, dan columns yang merupakan array of string yang berisi nama-nama kolom yang akan dipilih. Langkah yang dilakukan pada fungsi ini adalah sebagai berikut :

	1. Fungsi menggunakan fungsi sprintf untuk membentuk path file tabel yang akan dibaca, yaitu "%s/%s/%s", dengan DATABASE_ROOT sebagai direktori root database, db sebagai nama database, dan table sebagai nama tabel. Path tersebut disimpan dalam variabel filePath.

	2. Fungsi membuka file tabel dengan mode "r" (read) menggunakan fungsi fopen, dan file tersebut disimpan dalam variabel file.

	3. Fungsi menggunakan perulangan for untuk mencetak nama-nama kolom yang dipilih ke layar menggunakan printf. Perulangan ini akan mencetak nilai-nilai columns sesuai dengan columnsSize.

	4. Fungsi menggunakan fgets untuk membaca baris pertama dari file tabel yang berisi informasi kolom-kolom. Baris tersebut disimpan dalam variabel line.

	5. Setelah membaca baris pertama, file ditutup menggunakan fungsi fclose.

	6. Fungsi menggunakan perulangan for untuk mencari indeks (posisi) dari setiap kolom yang dipilih dalam baris pertama. Dalam setiap iterasi perulangan, fungsi menggunakan perulangan for dan strlen untuk memeriksa setiap karakter dalam baris pertama. Jika karakter tersebut adalah tanda '|' (pemisah kolom), maka nilai word akan diuji untuk melihat apakah mengandung nama kolom yang sesuai dengan columns[i]. Jika ditemukan, indeks kolom tersebut disimpan dalam array columnsIndex.
	
	7. Setelah selesai mencari indeks kolom-kolom yang dipilih, fungsi menggunakan fungsi sprintf untuk membentuk perintah yang akan menampilkan data yang sesuai dari file tabel. Perintah ini menggunakan perintah awk untuk memproses file dengan pemisah '|' dan mencetak nilai-nilai kolom yang sesuai. Format perintahnya akan bervariasi tergantung pada jumlah kolom yang dipilih.

	8. Perintah awk yang telah dibentuk akan dijalankan menggunakan fungsi system, dengan perintah disimpan dalam variabel cmd.

	Dengan langkah yang telah dijelaskan, fungsi selectQuery akan mencetak nilai-nilai kolom yang dipilih dari tabel ke layar.